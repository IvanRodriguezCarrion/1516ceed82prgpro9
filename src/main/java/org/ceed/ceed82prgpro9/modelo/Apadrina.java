package org.ceed.ceed82prgpro9.modelo;
import java.util.Date;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Apadrina  {
/**
 * Variables de clase (que verán todas las funciones del Apadrina
 y corresponden a los atributos de la tabla APADRINA
 */
  private String idapadrina;
  private String idpersona;
  private String idperro;
  private Padrino padrino;
  private Perro perro;
  private Date fecha;

 
 public Apadrina (String id, Padrino pa, Perro pe, Date date){
       idapadrina = id;
       padrino = pa;
       perro = pe;
       fecha = date;
 
 }

 public Apadrina() {
      
    }
 
    
// 
// /**
//     * Constructor de objetos tipo Apadrina que
//     * contiene las variables que le tenemos que pasar
//     * para definirlo.
//  */ 
// 
// public Apadrina (int numapadrina, int numpadrino, int numperro) { 
//       idapadrina = numapadrina;
//       Padrino idpadrino = numpadrino;
//       idperro = numperro;
//    }
 
 /**
     * @return the idApadrina
     * o método para devolver la idApadrina
     */
 
 public String getIdApadrina(){
   return idapadrina;
 } 
  public String getIdPersona(){
   return idpersona;
 } 
   public String getIdPerro(){
   return idperro;
 } 
 
 /**
     * @return the idApadrina
     * o método para devolver la idPadrino
     * de la clase Padrino
     */
 
 public Padrino getPadrino(){
   return padrino;
 }
 
  /**
     * @return the idPerro
     * o método para devolver el idPerro
     * de la clase Perro
     */
 
 public Perro getPerro(){
   return perro;
 }
 
 public Date getFecha() {
     return fecha;
 }
 
  /**
    * @param ida the idapadrina to set
    * o método para modificar la idapadrina
  */
    
    public void setIdApadrina(String ida) {
        idapadrina = ida;
    }
 
    public void setFecha(Date fetc) {
        fecha = fetc;
    }
    
  /**
    * @param padrino the padrino to set
    * o método para modificar la infor
  */
    
    public void setPadrino(Padrino padrino) {
        this.padrino = padrino;
    }     
 
  /**
    * @param perro the perro to set
    * o método para modificar la información de Perro 
    
  */
    
    public void setPerro(Perro perro) {
        this.perro = perro;
    } 
    
}