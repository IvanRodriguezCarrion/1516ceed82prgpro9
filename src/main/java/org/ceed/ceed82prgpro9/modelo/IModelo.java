package org.ceed.ceed82prgpro9.modelo;

import java.util.ArrayList;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/


public interface IModelo {

      	//Padrino
	public void create(Padrino padrino);
	public ArrayList <Padrino> readPadrino();
	public void update(Padrino padrino);
	public void delete(Padrino padrino);
	

	//Perro
	public void create(Perro perro);
	public ArrayList <Perro> readPerro();
	public void update(Perro perro);
	public void delete(Perro perro);
	

	//Apadrina
	public void create(Apadrina apadrina);
	public ArrayList <Apadrina> readApadrina();
	public void update(Apadrina apadrina);
	public void delete(Apadrina apadrina);
        
        //Genericos
        
        public void instalarDb();
        public void desinstalarDb();
        
}
