package org.ceed.ceed82prgpro9.vista;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class Verificador extends InputVerifier {
  
  private String variable;

  public Verificador(String variable) {
    this.variable = variable;
  }
  
  public static boolean validarMail(String s) {
    Pattern p = Pattern.compile("[\\w\\.]+@\\w+\\.\\w+");
    Matcher m = p.matcher(s);
    return m.matches();
  }
  
  public static boolean validarNChip (String s) {
    Pattern p = Pattern.compile("\\d{15}");
    Matcher m = p.matcher(s);
    return m.matches();
  }
  
  @Override
  public boolean verify(JComponent input) {
        if (input instanceof JTextField) {
            String texto = ((JTextField)input).getText();
            switch (variable) {
            case "tnchip": 
                if (!validarNChip(texto)) {
                    Funciones.mensaje(null,"El tamaño o caracteres del número de chip no son correctos. Introduzca 15 dígitos cualesquiera.", "VALIDACION DE NUMERO DE CHIP", 0);
                    return false;
                }
                break;
            case "temail": 
              if (!validarMail(texto)) {
                  Funciones.mensaje(null,"El mail introducido no es correcto. Éste debería tener el formato mail@mail.com .", "VALIDACION DEL EMAIL", 0);

                  return false;
              }
                  break;
           
            }   
        }
        return true;
    }
}