package org.ceed.ceed82prgpro9.vista;

import java.awt.FlowLayout; // Gestor de contenido tipo Flow
import java.awt.GridLayout; // Gestor de contenido tipo Border.
import javax.swing.JInternalFrame; // Poder crear J.Frames
import javax.swing.JPanel; // Para crear paneles
import javax.swing.JTable;
import javax.swing.JLabel; // Para crear textos
import javax.swing.JButton; // Para crear botones
import javax.swing.JComboBox; // Para crear combobox
import javax.swing.ImageIcon; // Permite insertar iconos.
import javax.swing.JTextField; // Permite insertar cajas detexto

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaGraficaPadrino extends JInternalFrame {
    private static VistaGraficaPadrino instancia;
    private JInternalFrame contenedorPrincipal = new JInternalFrame(); // Contedra todos los componentes de la vista principal
    private JPanel panelsuperior = new JPanel(); // Aquí ira la imagen de encabezado.
    private JPanel panelintermedio1 = new JPanel(); // Aquí iran los labels y textbox-
    private JPanel panelintermedio2 = new JPanel(); // Aquí iran los labels y textbox
    private JPanel panelinferior = new JPanel(); // Aquí iran los botones de aceptar y cancelar.
    private JTable tablaread = new JTable(); // Tabla para mostrar los reads
    private JLabel lblidpersona = new JLabel ("Identificador: ");
    private JTextField textidpersona = new JTextField(7);
    
    private JLabel lblnombre = new JLabel ("Nombre: ");
    private JTextField textnombre = new JTextField(10);
    
    private JLabel lbltelefono = new JLabel ("Telefono: ");
    private JTextField texttelefono = new JTextField(10);

    private JLabel lblemail = new JLabel ("Email: ");
    private JTextField textemail = new JTextField(10);
    
    private JButton botonAceptar = new JButton ("Aceptar");
    private JButton botonLimpiar = new JButton ("Limpiar");
   
    private JButton botonCreate = new JButton("Crear");
    private JButton botonRead = new JButton("Leer");
    private JButton botonUpdate = new JButton("Actualizar");
    private JButton botonDelete = new JButton("Borrar");
    private JButton botonCancelar = new JButton("Cancelar");
    private JButton botonSalir = new JButton("Salir");
    private JButton botonMoverIzq = new JButton("<");
    private JButton botonMoverDer = new JButton(">");
    private JButton botonMoverPri = new JButton("«");
    private JButton botonMoverUlt = new JButton("»");
//    private JButton botonComprobarId = new JButton("Comprobar identificador");
    
    private JLabel titulopadrino = new JLabel("  ");
    private ImageIcon imagePadrino = new ImageIcon(getClass().getResource("/imagenes/padrinocabecera1.png"));
    
    private JComboBox comboLeer = new JComboBox(); // con esto leeremos.
    
    private VistaGraficaPadrino() {
        menuAdmPadrinos();
    }
    
    
    private void menuAdmPadrinos() {
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Gestión de Padrinos");
        setSize(700,400);
        setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE); //Indicamos que la X en esta ventana no haga nada, ya que queremos que se cierre desde cancelar.
        setResizable(false); // Con esto evitamos que se redimensione la ventana
        GridLayout gridgeneral = new GridLayout(4,1,20,20);
        setLayout(gridgeneral);
        
        titulopadrino.setIcon(imagePadrino);//insertamos el titulo de la ventana
        panelsuperior.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5)); // PErmite insertar la cabecera en el centro
        panelsuperior.add(titulopadrino);
        panelsuperior.add(comboLeer);
        panelsuperior.add(botonMoverIzq);
        panelsuperior.add(botonMoverDer);
        
        panelintermedio1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 15)); // PErmite insertar los labels en el centro
        panelintermedio1.add(lblidpersona);
        panelintermedio1.add(textidpersona);
        panelintermedio1.add(lblnombre);
        panelintermedio1.add(textnombre);
        panelintermedio1.add(lbltelefono);
        panelintermedio1.add(texttelefono);
        panelintermedio1.add(lblemail);
        panelintermedio1.add(textemail);
        panelintermedio1.add(comboLeer);
        panelintermedio1.add(botonMoverPri);
        panelintermedio1.add(botonMoverIzq);
        panelintermedio1.add(botonMoverDer);
        panelintermedio1.add(botonMoverUlt);
//        panelintermedio1.add(botonComprobarId);
        
        panelintermedio2.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10)); // PErmite insertar los labels en el centro
        panelintermedio2.add(botonAceptar);
        panelintermedio2.add(botonLimpiar);
        panelintermedio2.add(botonCancelar);
        
        
        panelinferior.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 5));
        panelinferior.add(botonCreate);
        panelinferior.add(botonRead);
        panelinferior.add(botonUpdate);
        panelinferior.add(botonDelete);
        panelinferior.add(botonSalir);
        
        add(panelsuperior);
        add(panelintermedio1);
        add(panelintermedio2);
        add(panelinferior);
    
        }

    //getters para los botones y poder utilizarlos desde otras clases.
    public JButton getCreate(){
        return botonCreate;
    }
    public JButton getRead(){
        return botonRead;
    }
    public JButton getUpdate(){
        return botonUpdate;
    }
    public JButton getDelete(){
        return botonDelete;
    }
    public JButton getExit(){
        return botonSalir;
    }
    public JButton getAceptar(){
        return botonAceptar;
    }
    public JButton getLimpiar(){
        return botonLimpiar;
    }
    public JButton getBotonIzq(){
        return botonMoverIzq;
    }
    public JButton getBotonDer(){
        return botonMoverDer;
    }
    public JButton getCancelar(){
        return botonCancelar;
    }
//    public JButton getComprobarId(){
//        return botonComprobarId;
//    }
    public JButton getBotonPri(){
        return botonMoverPri;
    }
     public JButton getBotonUlt(){
        return botonMoverUlt;
    }
    
    public JLabel getLblId() {
        return lblidpersona;
    }
    

    
    //getters para los textfield de los que useremos sacar información.
    public JTextField getTextFieldIdPersona() {
        return textidpersona;
    }
    public JTextField getTextFieldNombre() {
        return textnombre;
    }
    public JTextField getTextFieldEmail() {
        return textemail;
    }
    public JTextField getTextFieldTelefono() {
        return texttelefono;
    }
    
    //Ahora el set del combobox
    public JComboBox getComboLeer() {
        return comboLeer;
    }
    
    public JInternalFrame getFrame() {
        return contenedorPrincipal;
    }
    
    public static VistaGraficaPadrino getInstancia() {
    if (instancia == null) {
       instancia = new VistaGraficaPadrino(); 
    } 
    return instancia;
    }
 }