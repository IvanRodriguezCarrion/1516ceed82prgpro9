package org.ceed.ceed82prgpro9.vista;

import java.awt.FlowLayout; // Gestor de contenido tipo Flow
import java.awt.GridLayout; // Gestor de contenido tipo Border.
import javax.swing.JInternalFrame; // Poder crear J.Frames
import javax.swing.JPanel; // Para crear paneles
import javax.swing.JTable;
import javax.swing.JLabel; // Para crear textos
import javax.swing.JButton; // Para crear botones
import javax.swing.JComboBox; // Para crear combobox
import javax.swing.ImageIcon; // Permite insertar iconos.
import javax.swing.JTextField; // Permite insertar cajas detexto
import com.toedter.calendar.JCalendar; //Permite traer JCalendar
import com.toedter.calendar.JDateChooser; //Permite traer JCalendar

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaGraficaApadrina extends JInternalFrame {
    private static VistaGraficaApadrina instancia;
    private JInternalFrame contenedorPrincipal = new JInternalFrame(); // Contedra todos los componentes de la vista principal
    private JPanel panelsuperior = new JPanel(); // Aquí ira la imagen de encabezado.
    private JPanel panelintermedio1 = new JPanel(); // Aquí iran los labels y textbox-
    private JPanel panelintermedio2 = new JPanel(); // Aquí iran los labels y textbox
    private JPanel panelintermedio3 = new JPanel(); // Aquí iran los labels y textbox
    private JPanel panelinferior = new JPanel(); // Aquí iran los botones de aceptar y cancelar.
    
    private JLabel lblidpersona = new JLabel ("ID padrino: ");
    private JTextField textidpersona = new JTextField(7);
    
    private JLabel lblidapadrina = new JLabel ("ID apadrinamiento: ");
    private JTextField textidapadrina = new JTextField(7);
     
    private JLabel lblidperro = new JLabel ("ID perro: ");
    private JTextField textidperro = new JTextField(7);

    private JLabel lblnombre = new JLabel ("Nombre: ");
    private JTextField textnombre = new JTextField(7);
    
    private JLabel lbltelefono = new JLabel ("Telefono: ");
    private JTextField texttelefono = new JTextField(7);

    private JLabel lblmascota = new JLabel ("Mascota: ");
    private JTextField textmascota = new JTextField(7);
    
    private JLabel lblNChip = new JLabel ("Nº Chip: ");
    private JTextField textNChip = new JTextField(7);
    
    private JButton botonAceptar = new JButton ("Aceptar");
    private JButton botonLimpiar = new JButton ("Limpiar");
    
    private JTextField textFecha = new JTextField(4);
    private JDateChooser botonFecha = new JDateChooser();
    
    private JButton botonCreate = new JButton("Crear");
    private JButton botonRead = new JButton("Leer");
    private JButton botonUpdate = new JButton("Actualizar");
    private JButton botonDelete = new JButton("Borrar");
    private JButton botonCancelar = new JButton("Cancelar");
    private JButton botonSalir = new JButton("Salir");
    private JButton botonMoverIzq = new JButton("<");
    private JButton botonMoverDer = new JButton(">");
    private JButton botonMoverPri = new JButton("«");
    private JButton botonMoverUlt = new JButton("»");
    private JButton botonComprobarId = new JButton("Comprobar identificador");
    
    private JLabel tituloapadrina = new JLabel("  ");
    private ImageIcon imagenapadrina = new ImageIcon(getClass().getResource("/imagenes/apadrinacabecera.png"));
    
    private JComboBox comboLeer = new JComboBox(); // con esto leeremos.
    private JComboBox comboLeerPadrinos = new JComboBox();
    private JComboBox comboLeerPerros = new JComboBox(); // con esto leeremos.
    
    private VistaGraficaApadrina() {
        menuAdmApadrina();
    }
    
    private void menuAdmApadrina() {
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Administración de Apadrinamiento");
        setSize(700,500);
        setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE); //Indicamos que la X en esta ventana no haga nada, ya que queremos que se cierre desde cancelar.
        setResizable(false); // Con esto evitamos que se redimensione la ventana
        GridLayout gridgeneral = new GridLayout(5,1,20,20);
        setLayout(gridgeneral);
        
        tituloapadrina.setIcon(imagenapadrina);//insertamos el titulo de la ventana
        panelsuperior.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5)); // PErmite insertar la cabecera en el centro
        panelsuperior.add(tituloapadrina);
              
        panelintermedio1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 15)); // PErmite insertar los labels en el centro
        panelintermedio1.add(textidperro);
        panelintermedio1.add(botonComprobarId);
        panelintermedio1.add(lblidapadrina);
        panelintermedio1.add(textidapadrina);
        panelintermedio1.add(lblidpersona);
        panelintermedio1.add(comboLeerPadrinos);
        panelintermedio1.add(lblidperro);
        panelintermedio1.add(comboLeerPerros);
        panelintermedio1.add(botonFecha);
        panelintermedio1.add(botonComprobarId);
        panelintermedio1.add(textidpersona);
        
        panelintermedio2.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10)); // PErmite insertar los labels en el centro
        panelintermedio2.add(lblnombre);
        panelintermedio2.add(textnombre);
        panelintermedio2.add(lbltelefono);
        panelintermedio2.add(texttelefono);
        panelintermedio2.add(comboLeer);
        panelintermedio2.add(lblmascota);
        panelintermedio2.add(textmascota);
        panelintermedio2.add(lblNChip);
        panelintermedio2.add(textNChip);

        
        panelintermedio2.add(botonMoverPri);
        panelintermedio2.add(botonMoverIzq);
        panelintermedio2.add(botonMoverDer);
        panelintermedio2.add(botonMoverUlt);
        
        panelintermedio3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 15)); // PErmite insertar los labels en el centro
        panelintermedio3.add(botonAceptar);
        panelintermedio3.add(botonLimpiar);
        panelintermedio3.add(botonCancelar);
        
        panelinferior.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 5));
        panelinferior.add(botonCreate);
        panelinferior.add(botonRead);
        panelinferior.add(botonUpdate);
        panelinferior.add(botonDelete);
        panelinferior.add(botonSalir);
        
        add(panelsuperior);
        add(panelintermedio1);
        add(panelintermedio2);
        add(panelintermedio3);
        add(panelinferior);
         
        }

    //getters para los botones y poder utilizarlos desde otras clases.
    public JButton getCreate(){
        return botonCreate;
    }
    public JButton getRead(){
        return botonRead;
    }
    public JButton getUpdate(){
        return botonUpdate;
    }
    public JButton getDelete(){
        return botonDelete;
    }
    public JButton getExit(){
        return botonSalir;
    }
    public JButton getAceptar(){
        return botonAceptar;
    }
    public JButton getLimpiar(){
        return botonLimpiar;
    }
    public JButton getBotonIzq(){
        return botonMoverIzq;
    }
    public JButton getBotonDer(){
        return botonMoverDer;
    }
    public JButton getCancelar(){
        return botonCancelar;
    }
    public JButton getComprobarId(){
        return botonComprobarId;
    }
    public JButton getBotonPri(){
        return botonMoverPri;
    }
     public JButton getBotonUlt(){
        return botonMoverUlt;
    }
     public JDateChooser getBotonFecha() {
         return botonFecha;
     }
    
    

    
    //getters para los textfield de los que useremos sacar información.
    public JTextField getTextFieldIdPersona() {
        return textidpersona;
    }
    
      public JTextField getTextFieldIdPerro() {
        return textidperro;
    }
      
    public JTextField getTextFieldIdApadrina() {
        return textidapadrina;
    }
    
    public JTextField getTextFieldNombre() {
        return textnombre;
    }

    public JTextField getTextFieldTelefono() {
        return texttelefono;
    }
    
    public JTextField getTextFieldMascota() {
        return textmascota;
    }
    public JTextField getTextFieldNChip() {
        return textNChip;
    }
    
    //Ahora el set del combobox
    public JComboBox getComboLeer() {
        return comboLeer;
    }
    
    public JComboBox getComboLeerPadrinos() {
        return comboLeerPadrinos;
    }
    
    public JComboBox getComboLeerPerros() {
        return comboLeerPerros;
    }
    
    public JInternalFrame getFrame() {
        return contenedorPrincipal;
    }
    
    public JPanel getPanelIntermedio1() {
        return panelintermedio1;
    }
    
    public JLabel getLblIdApadrina() {
        return lblidapadrina;
    }
    
    public static VistaGraficaApadrina getInstancia() {
    if (instancia == null) {
       instancia = new VistaGraficaApadrina(); 
    } 
    return instancia;
    }
    
 }