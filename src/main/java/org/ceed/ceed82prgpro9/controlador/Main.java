package org.ceed.ceed82prgpro9.controlador;
import org.ceed.ceed82prgpro9.vista.VistaGrafica;
import org.ceed.ceed82prgpro9.modelo.IModelo;
import org.ceed.ceed82prgpro9.modelo.ModeloMysql;
import java.util.InputMismatchException;
import org.ceed.ceed82prgpro9.modelo.ModeloDb4o;



/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Main {
        
  public static void main (String[] args) throws InputMismatchException {
   
   /**
    * La clase Main se encargará de controlar la ejecución
    * de los procesos del programa importando las clases
    * de los paquetes modelo y vista.
   */ 
    
        IModelo modelo = new ModeloDb4o();
	VistaGrafica vistagrafica = null;
	ControladorPrincipal cp = new ControladorPrincipal (modelo, vistagrafica);

    }
}  