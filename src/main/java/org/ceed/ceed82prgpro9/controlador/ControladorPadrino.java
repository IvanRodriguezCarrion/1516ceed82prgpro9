package org.ceed.ceed82prgpro9.controlador;

import org.ceed.ceed82prgpro9.modelo.Padrino;
import org.ceed.ceed82prgpro9.modelo.IModelo;
import org.ceed.ceed82prgpro9.modelo.ModeloMysql;
import org.ceed.ceed82prgpro9.vista.VistaGraficaPadrino;
import org.ceed.ceed82prgpro9.vista.Verificador;
import org.ceed.ceed82prgpro9.vista.Funciones;
import java.util.ArrayList;
import java.util.Iterator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import javax.swing.JOptionPane;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorPadrino implements ActionListener, MouseListener, ItemListener, FocusListener {
  
  private IModelo imodelo;
  private VistaGraficaPadrino vistagraficapadrino;
  private Padrino padrino = new Padrino();
  private Padrino padrino_indice = new Padrino();
  
  private String opcionEscogida;
  private boolean comprobarId,no_actualizar, no_campo_vacio, noCampoVacio;
  private int posicion;
  
  public ControladorPadrino(IModelo m, VistaGraficaPadrino v) {
    vistagraficapadrino = v;
    imodelo = m;    
    //Con estos escondemos aquello que sólo aparecerá con el read, update y delete
    inicioVentana();
    vistagraficapadrino.getTextFieldIdPersona().setVisible(false);
    vistagraficapadrino.getLblId().setVisible(false);
    vistagraficapadrino.getTextFieldIdPersona().setText("ola k ase");
    
    // Vigilamos los eventos sobre los botones
    vistagraficapadrino.getCreate().addActionListener(this);
    vistagraficapadrino.getRead().addActionListener(this);
    vistagraficapadrino.getUpdate().addActionListener(this);
    vistagraficapadrino.getDelete().addActionListener(this);
    vistagraficapadrino.getCancelar().addActionListener(this);
    vistagraficapadrino.getLimpiar().addActionListener(this);
    vistagraficapadrino.getExit().addActionListener(this);
    vistagraficapadrino.getAceptar().addActionListener(this);
//    vistagraficapadrino.getComprobarId().addActionListener(this);
    
    //Vigilamos los eventos sobre las flechas del cambio de combobox.
     vistagraficapadrino.getBotonIzq().addMouseListener(this);
     vistagraficapadrino.getBotonDer().addMouseListener(this);
     vistagraficapadrino.getBotonPri().addMouseListener(this);
     vistagraficapadrino.getBotonUlt().addMouseListener(this);
     
     //Vigilamos los eventos sobre el combobox mediante ItemListener
/*   EL ITEMLISTENER ESTÁ DENTRO DE LA FUNCION inicioVentana PARA PODER USARLO COMO
     FUNCIÓN DENTRO DE CANCEL Y PODER VACIAR EL COMBOBOX SIN QUE EL EVENTO DE CAMBIO
     DE ESTADO EMPIECE A PONER PROBLEMAS. ADEMÁS, EVITAMOS QUE SE PRODUZCA DUPLICIDAD.   
*/

     //Vigilamos que las cajas de texto ganen el focus.
     /*
     IGUAL QUE EN EL CASO ANTERIOR SÓLO QUE PARA EVITAR QUE LOS CAMPOS EN READ, UPDATE Y DELETE 
     SE BORREN SIN VENIR A CUENTO.
     */
      }

  private void create(){
        
        String id = vistagraficapadrino.getTextFieldIdPersona().getText();
        String nombre = vistagraficapadrino.getTextFieldNombre().getText();
        String telefono = vistagraficapadrino.getTextFieldTelefono().getText();
        String email = vistagraficapadrino.getTextFieldEmail().getText();
                
        Padrino padrino = new Padrino(id, nombre, telefono, email);
        
        imodelo.create(padrino);
        Funciones.mensaje(null,"Padrino creado con éxito.", "CREACIÓN DE PADRINOS", 1);
        vistagraficapadrino.getTextFieldIdPersona().setEditable(false);

     }
  
 
  private void read(){ // podria llamarse llenarCB perfectamente
      ArrayList ArrayListIteratorLeer = imodelo.readPadrino();
      Iterator padrinos_iterator_leer = ArrayListIteratorLeer.iterator(); 
      while (padrinos_iterator_leer.hasNext()) { 
        padrino = (Padrino) padrinos_iterator_leer.next(); 
        String id = padrino.getIdPersona();
        vistagraficapadrino.getComboLeer().addItem(id);
    } 
    
        
  }
  
  private void update() {
        String id = vistagraficapadrino.getTextFieldIdPersona().getText();
        String nombre = vistagraficapadrino.getTextFieldNombre().getText();
        String telefono = vistagraficapadrino.getTextFieldTelefono().getText();
        String email = vistagraficapadrino.getTextFieldEmail().getText();
        Padrino padrino = new Padrino(id, nombre, telefono, email);
        imodelo.update(padrino);
        JOptionPane.showMessageDialog(vistagraficapadrino.getFrame(),"Padrino actualizado con éxito.");
    }
   
  private void delete() {
        String id = vistagraficapadrino.getTextFieldIdPersona().getText();
        String nombre = vistagraficapadrino.getTextFieldNombre().getText();
        String telefono = vistagraficapadrino.getTextFieldTelefono().getText();
        String email = vistagraficapadrino.getTextFieldEmail().getText();
        Padrino padrino = new Padrino(id, nombre, telefono, email);
        imodelo.delete(padrino);
        JOptionPane.showMessageDialog(vistagraficapadrino.getFrame(),"Padrino borrado con éxito");    
    }
  
  private void llenadoTextBoxConComboBoxPadrino() {
    ArrayList ArrayIterator = imodelo.readPadrino();
    Iterator padrinos = ArrayIterator.iterator(); 
    Padrino padrino_iterator = new Padrino();
    while (padrinos.hasNext()) { 
      
      padrino_iterator = (Padrino) padrinos.next(); 
      String idComboBox = (String) vistagraficapadrino.getComboLeer().getSelectedItem();
      
      if (idComboBox.equals(padrino_iterator.getIdPersona())) {
          padrino = padrino_iterator;
        } 
     }
    vistagraficapadrino.getTextFieldIdPersona().setText(padrino.getIdPersona());
    vistagraficapadrino.getTextFieldNombre().setText(padrino.getNombre());
    vistagraficapadrino.getTextFieldEmail().setText(padrino.getEmail());
    vistagraficapadrino.getTextFieldTelefono().setText(padrino.getTelefono());
  }
   //Funciones para activar o desacrivar funcionalidades
   private void activarDesactivarBotones(boolean unouotro) { 
        vistagraficapadrino.getCreate().setEnabled(unouotro);
        vistagraficapadrino.getRead().setEnabled(unouotro);
        vistagraficapadrino.getUpdate().setEnabled(unouotro);
        vistagraficapadrino.getDelete().setEnabled(unouotro);
       
   }
    
   private void activarDesactivarLeer(boolean activoono) {
        vistagraficapadrino.getComboLeer().setVisible(activoono);
        vistagraficapadrino.getBotonIzq().setVisible(activoono);
        vistagraficapadrino.getBotonDer().setVisible(activoono);
        vistagraficapadrino.getBotonPri().setVisible(activoono);
        vistagraficapadrino.getBotonUlt().setVisible(activoono);
       
   }
   
   private void activarDesactivarCamposTexto(boolean activoono) {
        vistagraficapadrino.getTextFieldIdPersona().setEditable(activoono);
        vistagraficapadrino.getTextFieldNombre().setEditable(activoono);
        vistagraficapadrino.getTextFieldEmail().setEditable(activoono);
        vistagraficapadrino.getTextFieldTelefono().setEditable(activoono);
       
   }
   
   private void activarUpdateDelete(boolean activoono) {
        vistagraficapadrino.getComboLeer().setVisible(activoono);
        vistagraficapadrino.getBotonIzq().setVisible(activoono);
        vistagraficapadrino.getBotonDer().setVisible(activoono);
        vistagraficapadrino.getBotonPri().setVisible(activoono);
        vistagraficapadrino.getBotonUlt().setVisible(activoono);
        vistagraficapadrino.getTextFieldIdPersona().setEditable(false);
        vistagraficapadrino.getTextFieldNombre().setEditable(activoono);
        vistagraficapadrino.getTextFieldEmail().setEditable(activoono);
        vistagraficapadrino.getTextFieldTelefono().setEditable(activoono);
       
   }
  
    private void vaciarTodosCampos() {

         if (vistagraficapadrino.getComboLeer().getItemCount() > 0 ) {
            vistagraficapadrino.getComboLeer().removeAllItems();
         }
           vistagraficapadrino.getTextFieldIdPersona().setText("");
           vistagraficapadrino.getTextFieldNombre().setText("");
           vistagraficapadrino.getTextFieldEmail().setText("");
           vistagraficapadrino.getTextFieldTelefono().setText("");

      }
    private void activarDesactivarAceptarCancelar(boolean eleccion) {
       vistagraficapadrino.getAceptar().setEnabled(eleccion);
       vistagraficapadrino.getCancelar().setEnabled(eleccion);
       vistagraficapadrino.getLimpiar().setEnabled(eleccion);
    }
 
//Devuelve la aplicación a su estado original.
    private void cancelarTodo(){
        activarDesactivarBotones(true);
        activarDesactivarAceptarCancelar(false);
//        vistagraficapadrino.getAceptar().setVisible(true);
//        vistagraficapadrino.getComprobarId().setVisible(false);
        activarDesactivarCamposTexto(false);
        activarDesactivarLeer(false);

//        vistagraficapadrino.getTextFieldIdPersona().setText("ID");
        vistagraficapadrino.getTextFieldNombre().setText("Nombre");
        vistagraficapadrino.getTextFieldEmail().setText("mail@mail.com");
        vistagraficapadrino.getTextFieldTelefono().setText("Telefono");
    }
    
    private void inicioVentana() {
        vistagraficapadrino.getComboLeer().removeItemListener(this);
//        vistagraficapadrino.getTextFieldIdPersona().setText("ID");
        vistagraficapadrino.getTextFieldNombre().setText("Nombre");
        vistagraficapadrino.getTextFieldEmail().setText("mail@mail.com");
        vistagraficapadrino.getTextFieldTelefono().setText("Telefono");
        vistagraficapadrino.getComboLeer().removeAllItems();
        vistagraficapadrino.getCreate().setEnabled(true);
        vistagraficapadrino.getRead().setEnabled(true);
        vistagraficapadrino.getUpdate().setEnabled(true);
        vistagraficapadrino.getDelete().setEnabled(true);
//        vistagraficapadrino.getTextFieldIdPersona().setEditable(false);
        vistagraficapadrino.getTextFieldNombre().setEditable(false);
        vistagraficapadrino.getTextFieldEmail().setEditable(false);
        vistagraficapadrino.getTextFieldTelefono().setEditable(false);
        vistagraficapadrino.getComboLeer().setVisible(false);
        vistagraficapadrino.getBotonIzq().setVisible(false);
        vistagraficapadrino.getBotonDer().setVisible(false);
        vistagraficapadrino.getBotonPri().setVisible(false);
        vistagraficapadrino.getBotonUlt().setVisible(false);
        vistagraficapadrino.getAceptar().setEnabled(false);
        vistagraficapadrino.getCancelar().setEnabled(false);
        vistagraficapadrino.getLimpiar().setEnabled(false);
//        vistagraficapadrino.getComprobarId().setVisible(false);
        vistagraficapadrino.getCreate().requestFocus(true);
        vistagraficapadrino.getComboLeer().addItemListener(this);
        vistagraficapadrino.getTextFieldIdPersona().addFocusListener(this);
        vistagraficapadrino.getTextFieldNombre().addFocusListener(this);
        vistagraficapadrino.getTextFieldTelefono().addFocusListener(this);
        vistagraficapadrino.getTextFieldEmail().addFocusListener(this);
        
    }
    
//    private void settearCampos() {
//        vistagraficapadrino.getTextFieldIdPersona().setText("ID");
//        vistagraficapadrino.getTextFieldNombre().setText("Nombre");
//        vistagraficapadrino.getTextFieldEmail().setText("mail@mail.com");
//        vistagraficapadrino.getTextFieldTelefono().setText("Telefono");
//    
//    }
    private void limpiar(){
        vistagraficapadrino.getTextFieldNombre().setText("");
        vistagraficapadrino.getTextFieldEmail().setText("");
        vistagraficapadrino.getTextFieldTelefono().setText("");
    }
    
//    private void comprobarId() {
//
//    ArrayList arrayListComprobarId = imodelo.readPadrino();
//    Iterator padrinos = arrayListComprobarId.iterator(); 
//    
//    while (padrinos.hasNext()) { 
//      padrino = (Padrino) padrinos.next(); 
//      String id = vistagraficapadrino.getTextFieldIdPersona().getText();
//      
//      if (id.equals(padrino.getIdPersona())) {
//        JOptionPane.showMessageDialog(vistagraficapadrino.getFrame(),"ID encontrada, escoja otra.");
//        comprobarId = false;
//        } 
//     }
//    if (comprobarId) {
//        JOptionPane.showMessageDialog(vistagraficapadrino.getFrame(),"ID no encontrada");
//        vistagraficapadrino.getComprobarId().setVisible(false);
//        vistagraficapadrino.getTextFieldIdPersona().setEditable(false);
//        vistagraficapadrino.getAceptar().setVisible(true);
//        }
//    comprobarId = true;
//       
//    }
    
    private void comprobacionCamposVacios() {
        String id = vistagraficapadrino.getTextFieldIdPersona().getText();
        String nombre = vistagraficapadrino.getTextFieldNombre().getText();
        String telefono = vistagraficapadrino.getTextFieldTelefono().getText();
        String email = vistagraficapadrino.getTextFieldEmail().getText();
        noCampoVacio = true;
        
        while (id.equals("") || nombre.equals("") || telefono.equals("") || email.equals("")) {
            JOptionPane.showMessageDialog(vistagraficapadrino.getFrame(),"Uno de los campos está"
                    + " vacío, no se puede continuar.");
            noCampoVacio = false;
            break;
        }
    }
    
//     private void comprobacionMail() {
//       String email = vistagraficapadrino.getTextFieldEmail().getText();
//        Pattern pattern = Pattern.compile("[\\w\\.]+@\\w+\\.\\w+");
//        Matcher matcher = pattern.matcher(email);
//        campoMailCorrecto = true;
//        if (!matcher.matches()) {
//            JOptionPane.showMessageDialog(vistagraficapadrino.getFrame(),"El mail no es correcto, "
//                    + "ha de ser mail@mail.com");
//            campoMailCorrecto = false;
//        }
    
//    }
     
     
        private void verificarMail() {
        vistagraficapadrino.getTextFieldEmail().setInputVerifier(new Verificador("temail"));
        }
     
      private void anterior()  {
        ArrayList arrayListMover = imodelo.readPadrino();
        Iterator padrinos = arrayListMover.iterator(); 
    
        while (padrinos.hasNext()) { 
        padrino = (Padrino) padrinos.next(); 
        String id = vistagraficapadrino.getTextFieldIdPersona().getText();
      
      if (id.equals(padrino.getIdPersona())) {
            posicion = arrayListMover.indexOf(padrino);
        } 
     }
        if (posicion != 0) {
            posicion -= 1;
            padrino_indice = ((Padrino)arrayListMover.get(posicion));
            vistagraficapadrino.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrino.getTextFieldIdPersona().setText(padrino_indice.getIdPersona());
            vistagraficapadrino.getTextFieldNombre().setText(padrino_indice.getNombre());
            vistagraficapadrino.getTextFieldEmail().setText(padrino_indice.getEmail());
            vistagraficapadrino.getTextFieldTelefono().setText(padrino_indice.getTelefono());
        } else if (posicion == 0) {
            posicion = arrayListMover.size() - 1;
            padrino_indice = ((Padrino)arrayListMover.get(posicion));
            vistagraficapadrino.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrino.getTextFieldIdPersona().setText(padrino_indice.getIdPersona());
            vistagraficapadrino.getTextFieldNombre().setText(padrino_indice.getNombre());
            vistagraficapadrino.getTextFieldEmail().setText(padrino_indice.getEmail());
            vistagraficapadrino.getTextFieldTelefono().setText(padrino_indice.getTelefono());
        }
    }
      
     private void siguiente()  {
        ArrayList arrayListMover = imodelo.readPadrino();
        Iterator padrinos = arrayListMover.iterator(); 
    
        while (padrinos.hasNext()) { 
        padrino = (Padrino) padrinos.next(); 
        String id = vistagraficapadrino.getTextFieldIdPersona().getText();
      
      if (id.equals(padrino.getIdPersona())) {
            posicion = arrayListMover.indexOf(padrino);
        } 
     }
        if (posicion != arrayListMover.size() - 1) {
            posicion += 1;
            padrino_indice = ((Padrino)arrayListMover.get(posicion));
            vistagraficapadrino.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrino.getTextFieldIdPersona().setText(padrino_indice.getIdPersona());
            vistagraficapadrino.getTextFieldNombre().setText(padrino_indice.getNombre());
            vistagraficapadrino.getTextFieldEmail().setText(padrino_indice.getEmail());
            vistagraficapadrino.getTextFieldTelefono().setText(padrino_indice.getTelefono());
        } else {
            posicion = 0;
            padrino_indice = ((Padrino)arrayListMover.get(posicion));
            vistagraficapadrino.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrino.getTextFieldIdPersona().setText(padrino_indice.getIdPersona());
            vistagraficapadrino.getTextFieldNombre().setText(padrino_indice.getNombre());
            vistagraficapadrino.getTextFieldEmail().setText(padrino_indice.getEmail());
            vistagraficapadrino.getTextFieldTelefono().setText(padrino_indice.getTelefono());
        }
    }
     
     
     private void primer()  {
        ArrayList arrayListMover = imodelo.readPadrino();
        
        if (arrayListMover != null) {
            posicion = 0;
            padrino_indice = ((Padrino)arrayListMover.get(posicion));
            vistagraficapadrino.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrino.getTextFieldIdPersona().setText(padrino_indice.getIdPersona());
            vistagraficapadrino.getTextFieldNombre().setText(padrino_indice.getNombre());
            vistagraficapadrino.getTextFieldEmail().setText(padrino_indice.getEmail());
            vistagraficapadrino.getTextFieldTelefono().setText(padrino_indice.getTelefono());
        } else {
            padrino_indice = null;
            posicion = -1;
           
        }
    }
     
     private void ulti()  {
        ArrayList arrayListMover = imodelo.readPadrino(); 
            posicion = (arrayListMover.size()-1);
            padrino_indice = ((Padrino)arrayListMover.get(posicion));
            vistagraficapadrino.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrino.getTextFieldIdPersona().setText(padrino_indice.getIdPersona());
            vistagraficapadrino.getTextFieldNombre().setText(padrino_indice.getNombre());
            vistagraficapadrino.getTextFieldEmail().setText(padrino_indice.getEmail());
            vistagraficapadrino.getTextFieldTelefono().setText(padrino_indice.getTelefono());
    }
     
   @Override
   public void actionPerformed (ActionEvent event) { // Metodos implementados por ActionListener que vigila los botones
      if (vistagraficapadrino.getCreate() == event.getSource()) {
        
        activarDesactivarAceptarCancelar(true);
        activarDesactivarBotones(false);
        activarDesactivarCamposTexto(true);
        activarDesactivarLeer(false);
        verificarMail();
        ModeloMysql sq = new ModeloMysql();
//        vistagraficapadrino.getTextFieldIdPersona().setText(sq.idDisponiblePadrino());
        opcionEscogida = "crear";
        
      }
      else if (vistagraficapadrino.getRead() == event.getSource()) {
         no_actualizar = false;
          activarDesactivarLeer(true);
//        activarDesactivarBotones(false);
        vistagraficapadrino.getCancelar().setEnabled(true);
        vistagraficapadrino.getComboLeer().removeAllItems();
        read();
        llenadoTextBoxConComboBoxPadrino();
        opcionEscogida = "leer";
       no_actualizar = true;
      }
      else if (vistagraficapadrino.getUpdate() == event.getSource()) {
        activarDesactivarAceptarCancelar(true);
        activarUpdateDelete(true);
        activarDesactivarBotones(false);
//        llenadoTextBoxConComboBoxPadrino();
        read();
        opcionEscogida = "actualizar";
        
      }
      else if (vistagraficapadrino.getDelete() == event.getSource()) {
        activarDesactivarAceptarCancelar(true);
        activarUpdateDelete(true);
        activarDesactivarBotones(false);
//        llenadoTextBoxConComboBoxPadrino();
        read();
        opcionEscogida = "borrar";
      }
//      if (vistagraficapadrino.getComprobarId() == event.getSource()) {
// 
//      }
      else if (vistagraficapadrino.getExit() == event.getSource()) {
         vistagraficapadrino.dispose();
      }
       else if (vistagraficapadrino.getAceptar() == event.getSource()) {
         switch (opcionEscogida) {
             case "crear":
                 create();
                 break;
             case "actualizar":
                 update();
                 break;
             case "borrar":
                 delete();
                 break;    
            }
         }
      else if (vistagraficapadrino.getLimpiar() == event.getSource()) {
        limpiar();
      }
      else if (vistagraficapadrino.getCancelar() == event.getSource()) {
        inicioVentana();
      }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (vistagraficapadrino.getBotonIzq() == e.getSource()) {
        anterior();          
      }
      else if (vistagraficapadrino.getBotonDer() == e.getSource()) {
        siguiente();
      }
      else if (vistagraficapadrino.getBotonPri() == e.getSource()) {
        primer();
      }
      else if (vistagraficapadrino.getBotonUlt() == e.getSource()) {
        ulti();
      }
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (vistagraficapadrino.getComboLeer() == e.getSource()) {
            if (no_actualizar) {
        llenadoTextBoxConComboBoxPadrino();
            }
//        }
      }
    }

    @Override
    public void focusGained(FocusEvent e) {
        if (vistagraficapadrino.getTextFieldNombre() == e.getSource()) {
            vistagraficapadrino.getTextFieldNombre().setText("");
        }
        else if (vistagraficapadrino.getTextFieldTelefono() == e.getSource()) {
            vistagraficapadrino.getTextFieldTelefono().setText("");
        }
        else if (vistagraficapadrino.getTextFieldEmail() == e.getSource()) {
            vistagraficapadrino.getTextFieldEmail().setText("");
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 }
  
  

