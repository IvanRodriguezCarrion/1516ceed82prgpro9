package org.ceed.ceed82prgpro9.controlador;

import org.ceed.ceed82prgpro9.modelo.Padrino;
import org.ceed.ceed82prgpro9.modelo.Perro;
import org.ceed.ceed82prgpro9.modelo.Apadrina;
import org.ceed.ceed82prgpro9.modelo.IModelo;
import org.ceed.ceed82prgpro9.vista.VistaGraficaApadrina;
import org.ceed.ceed82prgpro9.vista.VistaGrafica;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;




/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorApadrina implements ActionListener, MouseListener, ItemListener, FocusListener {
  
  private IModelo imodelo;
  private VistaGrafica vistagrafica = new VistaGrafica();
  private VistaGraficaApadrina vistagraficapadrina;
  private Padrino padrino = new Padrino();
  private Perro perro = new Perro();
  private Apadrina apadrina = new Apadrina();
  private Apadrina apadrina_indice = new Apadrina();
  private String opcionEscogida;
  private boolean no_actualizar, no_actualizar_perros, no_actualizar_padrinos;
  private int posicion;
  private ArrayList padrinos, perros, apadrinas;
  
  public ControladorApadrina(IModelo m, VistaGraficaApadrina v) {
    vistagraficapadrina = v;
    imodelo = m;    
    //Con estos escondemos aquello que sólo aparecerá con el read, update y delete
    
    activarDesactivarBotones(true);
    activarDesactivarCamposTexto(false);
    activarDesactivarLeer(false);
    activarDesactivarAceptarCancelar(false);
    camposSoloLectura();
    vistagraficapadrina.getTextFieldIdPersona().setVisible(false);
    vistagraficapadrina.getTextFieldIdPerro().setVisible(false);
    vistagraficapadrina.getComprobarId().setVisible(false);
    vistagraficapadrina.getTextFieldIdApadrina().setVisible(false);
    vistagraficapadrina.getLblIdApadrina().setVisible(false);


    // Vigilamos los eventos sobre los botones
    vistagraficapadrina.getCreate().addActionListener(this);
    vistagraficapadrina.getRead().addActionListener(this);
    vistagraficapadrina.getUpdate().addActionListener(this);
    vistagraficapadrina.getDelete().addActionListener(this);
    vistagraficapadrina.getCancelar().addActionListener(this);
    vistagraficapadrina.getLimpiar().addActionListener(this);
    vistagraficapadrina.getExit().addActionListener(this);
    vistagraficapadrina.getAceptar().addActionListener(this);
    vistagraficapadrina.getComprobarId().addActionListener(this);
    
    //Vigilamos los eventos sobre las flechas del cambio de combobox.
     vistagraficapadrina.getBotonIzq().addMouseListener(this);
     vistagraficapadrina.getBotonDer().addMouseListener(this);
     vistagraficapadrina.getBotonPri().addMouseListener(this);
     vistagraficapadrina.getBotonUlt().addMouseListener(this);
    
     //Vigilamos los eventos sobre el combobox mediante ItemListener
     vistagraficapadrina.getComboLeer().addItemListener(this);
     vistagraficapadrina.getComboLeerPadrinos().addItemListener(this);
     vistagraficapadrina.getComboLeerPerros().addItemListener(this);
    
    //Vigilamos que la IDApadrina no se repita ni se quede vacía
     vistagraficapadrina.getTextFieldIdApadrina().addFocusListener(this);
      }
  
 


  private void create(){
       
        Apadrina apadrina = obtenerApadrinamiento();
        imodelo.create(apadrina);
        JOptionPane.showMessageDialog(vistagraficapadrina.getFrame(),"Apadrinamiento creado con éxito.");

     }
  
 
  private void read(){
    
    ArrayList ArrayListIteratorLeer = imodelo.readApadrina();
    Iterator apadrinas_iterator_leer = ArrayListIteratorLeer.iterator(); 
    while (apadrinas_iterator_leer.hasNext()) { 
      apadrina = (Apadrina) apadrinas_iterator_leer.next(); 
      String id = apadrina.getIdApadrina();
      String nombre = apadrina.getPadrino().getNombre();
      String nombre_perro = apadrina.getPerro().getNombre();
      vistagraficapadrina.getComboLeer().addItem(id + " - " + nombre + " - " + nombre_perro);
    } 
    
        
  }
  
  private void update() {
        Apadrina apadrina = obtenerApadrinamiento();
        imodelo.update(apadrina);
        JOptionPane.showMessageDialog(vistagraficapadrina.getFrame(),"Apadrinamiento actualizado con éxito.");
    }
   
  private void delete() {
        Apadrina apadrina = obtenerApadrinamiento();
        imodelo.delete(apadrina);
        
        JOptionPane.showMessageDialog(vistagraficapadrina.getFrame(),"Apadrinamiento borrado con éxito.");
    }
  
  private void llenadoTextBoxConComboBoxApadrina() {
    ArrayList ArrayIterator = imodelo.readApadrina();
    Iterator apadrinas = ArrayIterator.iterator(); 
    Apadrina apadrina_iterator;
    while (apadrinas.hasNext()) { 
      
      apadrina_iterator = (Apadrina) apadrinas.next(); 
      String idComboBox = (String) vistagraficapadrina.getComboLeer().getSelectedItem();
      
      if (idComboBox.equals(apadrina_iterator.getIdApadrina())) {
          apadrina = apadrina_iterator;
        } 
     }
    vistagraficapadrina.getTextFieldIdApadrina().setText(apadrina.getIdApadrina());
    vistagraficapadrina.getTextFieldIdPersona().setText(apadrina.getPadrino().getIdPersona());    
    vistagraficapadrina.getTextFieldIdPerro().setText(apadrina.getPerro().getIdPerro());
    vistagraficapadrina.getBotonFecha().setDate(apadrina.getFecha());
  }
  
  private void llenadoComboBoxPadrinoPerro() {
   //Llenado ComboBox Perro
    ArrayList ArrayListIteratorLeerPerros = imodelo.readPerro();
    Iterator perros_iterator_leer = ArrayListIteratorLeerPerros.iterator(); 
    while (perros_iterator_leer.hasNext()) { 
      perro = (Perro) perros_iterator_leer.next(); 
      String id = perro.getIdPerro();
      String nombre = perro.getNombre();
      vistagraficapadrina.getComboLeerPerros().addItem(id + " - " + nombre);
    }
    //Llenado ComboBox Padrinos
    ArrayList ArrayListIteratorLeerPadrinos = imodelo.readPadrino();
    Iterator padrinos_iterator_leer = ArrayListIteratorLeerPadrinos.iterator(); 
    while (padrinos_iterator_leer.hasNext()) { 
      padrino = (Padrino) padrinos_iterator_leer.next(); 
      String id = padrino.getIdPersona();
      String nombre = padrino.getNombre();
      vistagraficapadrina.getComboLeerPadrinos().addItem(id + " - " + nombre);
    } 
  }
   
  
  private void recuperarIdsPadrinoPerroComboBox() {
//      String recuperarIdPerro = (String) vistagraficapadrina.getComboLeerPerros().getSelectedItem();
//      String recuperarIdPersona = (String) vistagraficapadrina.getComboLeerPadrinos().getSelectedItem();
//      vistagraficapadrina.getTextFieldIdPersona().setText(recuperarIdPersona);
//      vistagraficapadrina.getTextFieldIdPerro().setText(recuperarIdPerro);
            
  }
  
  
   //Funciones para activar o desacrivar funcionalidades
  
   private void activarDesactivarBotones(boolean unouotro) { 
        vistagraficapadrina.getCreate().setEnabled(unouotro);
        vistagraficapadrina.getRead().setEnabled(unouotro);
        vistagraficapadrina.getUpdate().setEnabled(unouotro);
        vistagraficapadrina.getDelete().setEnabled(unouotro);
       
   }
    
   private void activarDesactivarLeer(boolean activoono) {
        vistagraficapadrina.getComboLeer().setVisible(activoono);
        vistagraficapadrina.getBotonIzq().setVisible(activoono);
        vistagraficapadrina.getBotonDer().setVisible(activoono);
        vistagraficapadrina.getBotonPri().setVisible(activoono);
        vistagraficapadrina.getBotonUlt().setVisible(activoono);
       
   }
   
   private void activarDesactivarCamposTexto(boolean activoono) {
        vistagraficapadrina.getTextFieldIdPersona().setEditable(activoono);
        vistagraficapadrina.getTextFieldIdPerro().setEditable(activoono);
        vistagraficapadrina.getTextFieldIdApadrina().setEditable(activoono);
       
   }
   
   private void activarUpdateDelete(boolean activoono) {
        vistagraficapadrina.getComboLeerPadrinos().setVisible(activoono);
        vistagraficapadrina.getComboLeerPerros().setVisible(activoono);
        vistagraficapadrina.getComboLeer().setVisible(activoono);
        vistagraficapadrina.getBotonIzq().setVisible(activoono);
        vistagraficapadrina.getBotonDer().setVisible(activoono);
        vistagraficapadrina.getBotonPri().setVisible(activoono);
        vistagraficapadrina.getBotonUlt().setVisible(activoono);
        vistagraficapadrina.getTextFieldIdApadrina().setEditable(false);
        vistagraficapadrina.getTextFieldIdPersona().setEditable(activoono);
        vistagraficapadrina.getTextFieldIdPerro().setEditable(activoono);
       
   }
  
    private void vaciarTodosCampos() {
        no_actualizar = false;
        no_actualizar_perros = false;
        no_actualizar_padrinos = false;
            vistagraficapadrina.getComboLeer().removeAllItems();
            vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
            vistagraficapadrina.getComboLeerPerros().removeAllItems();
         no_actualizar_perros = true;
         no_actualizar_padrinos = true;
         no_actualizar = true;
        limpiar();
      }
    private void activarDesactivarAceptarCancelar(boolean eleccion) {
       vistagraficapadrina.getAceptar().setEnabled(eleccion);
       vistagraficapadrina.getCancelar().setEnabled(eleccion);
       vistagraficapadrina.getLimpiar().setEnabled(eleccion);
    }
 
//Devuelve la aplicación a su estado original.
    private void cancelarTodo(){
        activarDesactivarBotones(true);
        activarDesactivarAceptarCancelar(false);
        vistagraficapadrina.getAceptar().setVisible(true);
        activarDesactivarCamposTexto(false);
        activarDesactivarLeer(false);
        vaciarTodosCampos();
        vistagraficapadrina.getTextFieldIdApadrina().setVisible(false);
        vistagraficapadrina.getLblIdApadrina().setVisible(false);
    }
  
    
    private void limpiar(){

        vistagraficapadrina.getTextFieldIdApadrina().setText("");
        vistagraficapadrina.getTextFieldNombre().setText("");
        vistagraficapadrina.getTextFieldTelefono().setText("");
        vistagraficapadrina.getTextFieldMascota().setText("");
        vistagraficapadrina.getTextFieldNChip().setText("");
    }
    
    
    private void camposSoloLectura() {
        vistagraficapadrina.getTextFieldNombre().setEditable(false);
        vistagraficapadrina.getTextFieldMascota().setEditable(false);
        vistagraficapadrina.getTextFieldNChip().setEditable(false);
        vistagraficapadrina.getTextFieldTelefono().setEditable(false);
    }
    
    private void anterior()  {
        ArrayList arrayListMover = imodelo.readApadrina();
        Iterator apadrinas = arrayListMover.iterator(); 
    
        while (apadrinas.hasNext()) { 
        apadrina = (Apadrina) apadrinas.next(); 
        String id = vistagraficapadrina.getTextFieldIdApadrina().getText();
      
      if (id.equals(apadrina.getIdApadrina())) {
            posicion = arrayListMover.indexOf(apadrina);
        } 
     }
        if (posicion != 0) {
            posicion -= 1;
            apadrina_indice = ((Apadrina)arrayListMover.get(posicion));
            vistagraficapadrina.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrina.getTextFieldIdPersona().setText(apadrina_indice.getPadrino().getIdPersona());
            vistagraficapadrina.getTextFieldIdPerro().setText(apadrina_indice.getPerro().getIdPerro());
            vistagraficapadrina.getTextFieldIdApadrina().setText(apadrina_indice.getIdApadrina());
            vistagraficapadrina.getBotonFecha().setDate(apadrina_indice.getFecha());
        } else {
            posicion = arrayListMover.size() - 1;
            apadrina_indice = ((Apadrina)arrayListMover.get(posicion));
            vistagraficapadrina.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrina.getTextFieldIdPersona().setText(apadrina_indice.getPadrino().getIdPersona());
            vistagraficapadrina.getTextFieldIdPerro().setText(apadrina_indice.getPerro().getIdPerro());
            vistagraficapadrina.getTextFieldIdApadrina().setText(apadrina_indice.getIdApadrina());
            vistagraficapadrina.getBotonFecha().setDate(apadrina_indice.getFecha());
        }
    }
      
     private void siguiente()  {
        ArrayList arrayListMover = imodelo.readApadrina();
        Iterator apadrinas = arrayListMover.iterator(); 
    
        while (apadrinas.hasNext()) { 
        apadrina = (Apadrina) apadrinas.next(); 
        String id = vistagraficapadrina.getTextFieldIdApadrina().getText();
      
      if (id.equals(apadrina.getIdApadrina())) {
            posicion = arrayListMover.indexOf(apadrina);
        } 
     }
        if (posicion != arrayListMover.size() - 1) {
            posicion += 1;
            apadrina_indice = ((Apadrina)arrayListMover.get(posicion));
            vistagraficapadrina.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrina.getTextFieldIdPersona().setText(apadrina_indice.getPadrino().getIdPersona());
            vistagraficapadrina.getTextFieldIdPerro().setText(apadrina_indice.getPerro().getIdPerro());
            vistagraficapadrina.getTextFieldIdApadrina().setText(apadrina_indice.getIdApadrina());
            vistagraficapadrina.getBotonFecha().setDate(apadrina_indice.getFecha());
        } else {
            posicion = 0;
            apadrina_indice = ((Apadrina)arrayListMover.get(posicion));
            vistagraficapadrina.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrina.getTextFieldIdPersona().setText(apadrina_indice.getPadrino().getIdPersona());
            vistagraficapadrina.getTextFieldIdPerro().setText(apadrina_indice.getPerro().getIdPerro());
            vistagraficapadrina.getTextFieldIdApadrina().setText(apadrina_indice.getIdApadrina());
            vistagraficapadrina.getBotonFecha().setDate(apadrina_indice.getFecha());
        }
    }

    private void primer()  {
        ArrayList arrayListMover = imodelo.readApadrina();
        
        if (arrayListMover != null) {
            posicion = 0;
            apadrina_indice = ((Apadrina)arrayListMover.get(posicion));
            vistagraficapadrina.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrina.getTextFieldIdPersona().setText(apadrina_indice.getPadrino().getIdPersona());
            vistagraficapadrina.getTextFieldIdPerro().setText(apadrina_indice.getPerro().getIdPerro());
            vistagraficapadrina.getTextFieldIdApadrina().setText(apadrina_indice.getIdApadrina());
            vistagraficapadrina.getBotonFecha().setDate(apadrina_indice.getFecha());
        } else {
            apadrina_indice = null;
            posicion = -1;
           
        }
    }
     
     private void ulti()  {
        ArrayList arrayListMover = imodelo.readApadrina(); 
            posicion = (arrayListMover.size()-1);
            apadrina_indice = ((Apadrina)arrayListMover.get(posicion));
            vistagraficapadrina.getComboLeer().setSelectedIndex(posicion);
            vistagraficapadrina.getTextFieldIdPersona().setText(apadrina_indice.getPadrino().getIdPersona());
            vistagraficapadrina.getTextFieldIdPerro().setText(apadrina_indice.getPerro().getIdPerro());
            vistagraficapadrina.getTextFieldIdApadrina().setText(apadrina_indice.getIdApadrina());
            vistagraficapadrina.getBotonFecha().setDate(apadrina_indice.getFecha());
    }
     
     private void activarDesactivarEventosActualizacionCB(boolean eleccion) {
         no_actualizar_perros = eleccion;
        no_actualizar_padrinos = eleccion;
        no_actualizar = eleccion;  
    }
     
   @Override
   public void actionPerformed (ActionEvent event) { // Metodos implementados por ActionListener que vigila los botones
      if (vistagraficapadrina.getCreate() == event.getSource()) {
 
        vistagraficapadrina.getComboLeerPerros().removeAllItems();
        vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
        vistagraficapadrina.getComboLeerPerros().setEnabled(true);
        vistagraficapadrina.getComboLeerPadrinos().setEnabled(true);
        vistagraficapadrina.getBotonFecha().setEnabled(true);
        vistagraficapadrina.getComboLeer().removeItemListener(this);
        vistagraficapadrina.getComboLeerPadrinos().removeItemListener(this);
        vistagraficapadrina.getComboLeerPerros().removeItemListener(this);
        vistagraficapadrina.getComboLeer().addItemListener(this);
        vistagraficapadrina.getComboLeerPadrinos().addItemListener(this);
        vistagraficapadrina.getComboLeerPerros().addItemListener(this);
        no_actualizar_perros = true;
        no_actualizar_padrinos = true;
        llenadoComboBoxPadrinoPerro();
        recuperarIdsPadrinoPerroComboBox();
        activarDesactivarAceptarCancelar(true);
        activarDesactivarBotones(false);
        activarDesactivarCamposTexto(true);
        activarDesactivarLeer(false);
        vistagraficapadrina.getTextFieldIdApadrina().setVisible(false);
        vistagraficapadrina.getLblIdApadrina().setVisible(false);
        opcionEscogida = "crear";

      }
      else if (vistagraficapadrina.getRead() == event.getSource()) {
        vistagraficapadrina.getTextFieldIdApadrina().setVisible(true);
        vistagraficapadrina.getLblIdApadrina().setVisible(true);
        no_actualizar = false;
        no_actualizar_perros = false;
        no_actualizar_padrinos = false;
        activarDesactivarBotones(false);
        vistagraficapadrina.getComboLeerPerros().removeAllItems();
        vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
        vistagraficapadrina.getComboLeerPerros().setEnabled(false);
        vistagraficapadrina.getComboLeerPadrinos().setEnabled(false);
        vistagraficapadrina.getBotonFecha().setEnabled(false);
        llenadoComboBoxPadrinoPerro();
        activarDesactivarLeer(true);
        vistagraficapadrina.getCancelar().setEnabled(true);
        read();
        llenadoTextBoxConComboBoxApadrina();
        opcionEscogida = "leer";
        no_actualizar = true;
        no_actualizar_perros = true;
        no_actualizar_padrinos = true;
      }
      else if (vistagraficapadrina.getUpdate() == event.getSource()) {
        vistagraficapadrina.getTextFieldIdApadrina().setVisible(true);
        vistagraficapadrina.getLblIdApadrina().setVisible(true);
        no_actualizar_perros = false;
        no_actualizar_padrinos = false;
        no_actualizar = false;  
        vistagraficapadrina.getComboLeerPerros().removeAllItems();
        vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
        vistagraficapadrina.getComboLeerPerros().setEnabled(true);
        vistagraficapadrina.getComboLeerPadrinos().setEnabled(true);
//        vistagraficapadrina.getTextFieldIdApadrina().setVisible(false);
        read();
        llenadoTextBoxConComboBoxApadrina();
        llenadoComboBoxPadrinoPerro();
        recuperarIdsPadrinoPerroComboBox();
        activarDesactivarAceptarCancelar(true);
        activarDesactivarBotones(false);
        activarDesactivarCamposTexto(true);
        activarDesactivarLeer(true);
        vistagraficapadrina.getTextFieldIdApadrina().setEnabled(false);
        vistagraficapadrina.getBotonFecha().setEnabled(true);
        opcionEscogida = "actualizar";
        no_actualizar = true;
        no_actualizar_perros = true;
        no_actualizar_padrinos = true;
      }
      else if (vistagraficapadrina.getDelete() == event.getSource()) {
        vistagraficapadrina.getTextFieldIdApadrina().setVisible(true);
        vistagraficapadrina.getLblIdApadrina().setVisible(true);
        no_actualizar_perros = false;
        no_actualizar_padrinos = false;
        no_actualizar = false;
        activarDesactivarEventosActualizacionCB(false);
        activarDesactivarAceptarCancelar(true);
        activarUpdateDelete(true);
        activarDesactivarBotones(false);
        vistagraficapadrina.getComboLeerPerros().removeAllItems();
        vistagraficapadrina.getComboLeerPadrinos().removeAllItems();
        vistagraficapadrina.getComboLeerPerros().setEnabled(false);
        vistagraficapadrina.getComboLeerPadrinos().setEnabled(false);
        vistagraficapadrina.getTextFieldIdApadrina().setEnabled(false);
        vistagraficapadrina.getBotonFecha().setEnabled(false);
        read();
        llenadoTextBoxConComboBoxApadrina();
        llenadoComboBoxPadrinoPerro();
        recuperarIdsPadrinoPerroComboBox();
        activarDesactivarAceptarCancelar(true);
        activarDesactivarBotones(false);
        activarDesactivarCamposTexto(true);
        activarDesactivarLeer(true);
        opcionEscogida = "borrar";
        activarDesactivarEventosActualizacionCB(true);
        no_actualizar_perros = true;
        no_actualizar_padrinos = true;
        no_actualizar = true;
      }
      else if (vistagraficapadrina.getExit() == event.getSource()) {
         vistagraficapadrina.dispose();
      }
       else if (vistagraficapadrina.getAceptar() == event.getSource()) {
         switch (opcionEscogida) {
             case "crear":
                 no_actualizar = false;            
                 create();
                 no_actualizar = true;
                 break;
             case "actualizar":
                 update();
                 break;
             case "borrar":
                 delete();
                 break;    
            }
         }
      else if (vistagraficapadrina.getLimpiar() == event.getSource()) {
        limpiar();
      }
      else if (vistagraficapadrina.getCancelar() == event.getSource()) {
        cancelarTodo();
      }
    }
   
   private Apadrina obtenerApadrinamiento() {
        perro = new Perro();
        padrino = new Padrino();
        
        String idapa = vistagraficapadrina.getTextFieldIdApadrina().getText();
        padrino.setIdPersona(vistagraficapadrina.getTextFieldIdPersona().getText());
        perro.setIdPerro(vistagraficapadrina.getTextFieldIdPerro().getText());
        Date fecha = (Date) vistagraficapadrina.getBotonFecha().getDate();
                
        Apadrina apadrina = new Apadrina(idapa, padrino, perro, fecha);
        return apadrina;
   }
   

    @Override
    public void mouseClicked(MouseEvent e) {
        if (vistagraficapadrina.getBotonIzq() == e.getSource()) {
        anterior();          
      }
      else if (vistagraficapadrina.getBotonDer() == e.getSource()) {
        siguiente();
      }
      else if (vistagraficapadrina.getBotonPri() == e.getSource()) {
        primer();
      }
      else if (vistagraficapadrina.getBotonUlt() == e.getSource()) {
        ulti();
      }
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
     public void itemStateChanged(ItemEvent e) {
        if (vistagraficapadrina.getComboLeer() == e.getSource()) {
            if (no_actualizar) {
                cambioCBApadrina();
                }
            }
        else if (vistagraficapadrina.getComboLeerPadrinos() == e.getSource()) {
            if (no_actualizar_padrinos){
                cambioCBPadrinos();
             }
        }
        else if (vistagraficapadrina.getComboLeerPerros() == e.getSource()) {
            if (no_actualizar_perros){
                cambioCBPerros();
                
            }
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent e) {
//        if (vistagraficapadrina.getTextFieldIdApadrina() == e.getSource()) {
//            comprobarId();
//            }
    }
    
    private void cambioCBApadrina() {
        String id;
                Iterator it;
                Apadrina apadrina;
                String idCombo = (String)vistagraficapadrina.getComboLeer().getSelectedItem();
                StringTokenizer stokenizer = new StringTokenizer(idCombo, " - ");
                id = stokenizer.nextToken();
                apadrinas = imodelo.readApadrina();
                it = apadrinas.iterator(); 
                while (it.hasNext()) { 
                    apadrina = (Apadrina) it.next(); 
                    if (apadrina.getIdApadrina().equals(id)) {
                        vistagraficapadrina.getTextFieldIdApadrina().setText(apadrina.getIdApadrina());
                        vistagraficapadrina.getTextFieldMascota().setText(apadrina.getPerro().getNombre());
                        vistagraficapadrina.getTextFieldNChip().setText(apadrina.getPerro().getNChip());
                        vistagraficapadrina.getTextFieldIdPersona().setText(apadrina.getPadrino().getIdPersona());
                        vistagraficapadrina.getTextFieldTelefono().setText(apadrina.getPadrino().getTelefono());
                        vistagraficapadrina.getTextFieldIdPersona().setText(apadrina.getPadrino().getIdPersona());
                        vistagraficapadrina.getTextFieldIdPerro().setText(apadrina.getPerro().getIdPerro());
                    }
                }
                String recuperarIdPersona = vistagraficapadrina.getTextFieldIdPersona().getText();
                ArrayList arrayListBuscarIdPadrino = imodelo.readPadrino();
                Iterator padrinos = arrayListBuscarIdPadrino.iterator(); 

                    while (padrinos.hasNext()) { 
                        padrino = (Padrino) padrinos.next(); 
                           if (recuperarIdPersona.equals(padrino.getIdPersona())) {
                              int posicion_padrino = arrayListBuscarIdPadrino.indexOf(padrino);
                              vistagraficapadrina.getComboLeerPadrinos().setSelectedIndex(posicion_padrino);
                            } 
                    }
                
                String recuperarIdPerro = vistagraficapadrina.getTextFieldIdPerro().getText();
                ArrayList arrayListBuscarIdPerro = imodelo.readPerro();
                Iterator perros = arrayListBuscarIdPerro.iterator(); 

                    while (perros.hasNext()) { 
                        perro = (Perro) perros.next(); 
                           if (recuperarIdPerro.equals(perro.getIdPerro())) {
                              int posicion_perro = arrayListBuscarIdPerro.indexOf(perro);
                              vistagraficapadrina.getComboLeerPerros().setSelectedIndex(posicion_perro);
                            } 
                    }
    
    }
    
    private void cambioCBPadrinos() {
        String id;
                Iterator it;
                Padrino padrino;
                String idCombo = (String)vistagraficapadrina.getComboLeerPadrinos().getSelectedItem();
                StringTokenizer stokenizer = new StringTokenizer(idCombo, " - ");
                id = stokenizer.nextToken();
                padrinos = imodelo.readPadrino();
                it = padrinos.iterator(); 
                while (it.hasNext()) { 
                    padrino = (Padrino) it.next(); 
                    if (padrino.getIdPersona().equals(id)) {
                        vistagraficapadrina.getTextFieldIdPersona().setText(padrino.getIdPersona());
                        vistagraficapadrina.getTextFieldNombre().setText(padrino.getNombre());
                        vistagraficapadrina.getTextFieldTelefono().setText(padrino.getTelefono());
                    }
                }
    }
    
    private void cambioCBPerros() {
          String id;
                Iterator it;
                Perro perro;
                String idCombo = (String)vistagraficapadrina.getComboLeerPerros().getSelectedItem();
                StringTokenizer stokenizer = new StringTokenizer(idCombo, " - ");
                id = stokenizer.nextToken();
                perros = imodelo.readPerro();
                it = perros.iterator(); 
                while (it.hasNext()) { 
                    perro = (Perro) it.next(); 
                    if (perro.getIdPerro().equals(id)) {
                        vistagraficapadrina.getTextFieldIdPerro().setText(perro.getIdPerro());
                        vistagraficapadrina.getTextFieldMascota().setText(perro.getNombre());
                        vistagraficapadrina.getTextFieldNChip().setText(perro.getNChip());
                    }
                }  
    }
    
}
  
  

