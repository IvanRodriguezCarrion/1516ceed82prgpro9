package org.ceed.ceed82prgpro9.controlador;

import org.ceed.ceed82prgpro9.modelo.Perro;
import org.ceed.ceed82prgpro9.modelo.IModelo;
import org.ceed.ceed82prgpro9.vista.VistaGraficaPerro;
import org.ceed.ceed82prgpro9.vista.VistaGrafica;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JOptionPane;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorPerro implements ActionListener, MouseListener, ItemListener {
  
  private IModelo imodelo;
  private VistaGraficaPerro vistagraficaperro;
  private Perro perro = new Perro();
  private Perro perro_indice = new Perro();
  
  private String opcionEscogida;
  private boolean comprobarId, noCampoVacio, campoMailCorrecto,no_actualizar;
  private int posicion;
  
  public ControladorPerro(IModelo m, VistaGraficaPerro v) {
    vistagraficaperro = v;
    imodelo = m;
    
    //Con estos escondemos aquello que sólo aparecerá con el read, update y delete
    vistagraficaperro.getTextFieldIdPerro().setVisible(false);
    vistagraficaperro.getIdLbl().setVisible(false);
    vistagraficaperro.getComprobarId().setVisible(false);
    settearCampos();
    activarDesactivarBotones(true);
    activarDesactivarCamposTexto(false);
    activarDesactivarLeer(false);
    activarDesactivarAceptarCancelar(false);
//    vistagraficaperro.getComprobarId().setVisible(false);

    // Vigilamos los eventos sobre los botones
    vistagraficaperro.getCreate().addActionListener(this);
    vistagraficaperro.getRead().addActionListener(this);
    vistagraficaperro.getUpdate().addActionListener(this);
    vistagraficaperro.getDelete().addActionListener(this);
    vistagraficaperro.getCancelar().addActionListener(this);
    vistagraficaperro.getLimpiar().addActionListener(this);
    vistagraficaperro.getExit().addActionListener(this);
    vistagraficaperro.getAceptar().addActionListener(this);
//    vistagraficaperro.getComprobarId().addActionListener(this);
    
    //Vigilamos los eventos sobre las flechas del cambio de combobox.
     vistagraficaperro.getBotonIzq().addMouseListener(this);
     vistagraficaperro.getBotonDer().addMouseListener(this);
     vistagraficaperro.getBotonPri().addMouseListener(this);
     vistagraficaperro.getBotonUlt().addMouseListener(this);
     
    
     //Vigilamos los eventos sobre el combobox mediante ItemListener
     vistagraficaperro.getComboLeer().addItemListener(this);
      }
  
  


  private void create(){
        
        String id = vistagraficaperro.getTextFieldIdPerro().getText();
        String nombre = vistagraficaperro.getTextFieldNombre().getText();
        String nchip = vistagraficaperro.getTextFieldNChip().getText();
        String raza = vistagraficaperro.getTextFieldRaza().getText();
                
        Perro perro = new Perro(id, nombre, nchip, raza);
        imodelo.create(perro);
        JOptionPane.showMessageDialog(vistagraficaperro.getFrame(),"Perro creado con éxito.");
        vistagraficaperro.getTextFieldIdPerro().setEditable(false);

     }
  
 
  private void read(){
    ArrayList ArrayListIteratorLeer = imodelo.readPerro();
    Iterator perros_iterator_leer = ArrayListIteratorLeer.iterator(); 
    while (perros_iterator_leer.hasNext()) { 
      perro = (Perro) perros_iterator_leer.next(); 
      String id = perro.getIdPerro();
      vistagraficaperro.getComboLeer().addItem(id);
    } 
    
        
  }
  
  private void update() {
         String id = vistagraficaperro.getTextFieldIdPerro().getText();
        String nombre = vistagraficaperro.getTextFieldNombre().getText();
        String nchip = vistagraficaperro.getTextFieldNChip().getText();
        String raza = vistagraficaperro.getTextFieldRaza().getText();
                
        Perro perro = new Perro(id, nombre, nchip, raza);
        imodelo.update(perro);
        JOptionPane.showMessageDialog(vistagraficaperro.getFrame(),"Perro editado con éxito.");

    }
   
  private void delete() {
        String id = vistagraficaperro.getTextFieldIdPerro().getText();
        String nombre = vistagraficaperro.getTextFieldNombre().getText();
        String nchip = vistagraficaperro.getTextFieldNChip().getText();
        String raza = vistagraficaperro.getTextFieldRaza().getText();
                
        Perro perro = new Perro(id, nombre, nchip, raza);
        imodelo.delete(perro);
        JOptionPane.showMessageDialog(vistagraficaperro.getFrame(),"Perro eliminado con éxito.");
        vistagraficaperro.getTextFieldIdPerro().setEditable(false);
    
    }
  
  private void llenadoTextBoxConComboBoxPerro() {
    ArrayList ArrayIterator = imodelo.readPerro();
    Iterator perros = ArrayIterator.iterator(); 
    Perro perro_iterator = new Perro();
    while (perros.hasNext()) { 
      
      perro_iterator = (Perro) perros.next(); 
      String idComboBox = (String) vistagraficaperro.getComboLeer().getSelectedItem();
      
      if (idComboBox.equals(perro_iterator.getIdPerro())) {
          perro = perro_iterator;
        } 
     }
    vistagraficaperro.getTextFieldIdPerro().setText(perro.getIdPerro());
    vistagraficaperro.getTextFieldNombre().setText(perro.getNombre());
    vistagraficaperro.getTextFieldNChip().setText(perro.getNChip());
    vistagraficaperro.getTextFieldRaza().setText(perro.getRaza());
  }
   //Funciones para activar o desacrivar funcionalidades
   private void activarDesactivarBotones(boolean unouotro) { 
        vistagraficaperro.getCreate().setEnabled(unouotro);
        vistagraficaperro.getRead().setEnabled(unouotro);
        vistagraficaperro.getUpdate().setEnabled(unouotro);
        vistagraficaperro.getDelete().setEnabled(unouotro);
       
   }
    
   private void activarDesactivarLeer(boolean activoono) {
        vistagraficaperro.getComboLeer().setVisible(activoono);
        vistagraficaperro.getBotonIzq().setVisible(activoono);
        vistagraficaperro.getBotonDer().setVisible(activoono);
        vistagraficaperro.getBotonPri().setVisible(activoono);
        vistagraficaperro.getBotonUlt().setVisible(activoono);       
   }
   
   private void activarDesactivarCamposTexto(boolean activoono) {
//        vistagraficaperro.getTextFieldIdPerro().setEditable(activoono);
        vistagraficaperro.getTextFieldNombre().setEditable(activoono);
        vistagraficaperro.getTextFieldNChip().setEditable(activoono);
        vistagraficaperro.getTextFieldRaza().setEditable(activoono);
       
   }
   
   private void activarUpdateDelete(boolean activoono) {
        vistagraficaperro.getComboLeer().setVisible(activoono);
        vistagraficaperro.getBotonIzq().setVisible(activoono);
        vistagraficaperro.getBotonDer().setVisible(activoono);
        vistagraficaperro.getBotonPri().setVisible(activoono);
        vistagraficaperro.getBotonUlt().setVisible(activoono);
        vistagraficaperro.getTextFieldIdPerro().setEditable(false);
        vistagraficaperro.getTextFieldNombre().setEditable(activoono);
        vistagraficaperro.getTextFieldNChip().setEditable(activoono);
        vistagraficaperro.getTextFieldRaza().setEditable(activoono);
       
   }
  
    private void vaciarTodosCampos() {

         if (vistagraficaperro.getComboLeer().getItemCount() > 0 ) {
            vistagraficaperro.getComboLeer().removeAllItems();
         }
//           vistagraficaperro.getTextFieldIdPerro().setText("");
           vistagraficaperro.getTextFieldNombre().setText("");
           vistagraficaperro.getTextFieldNChip().setText("");
           vistagraficaperro.getTextFieldRaza().setText("");

      }
    private void activarDesactivarAceptarCancelar(boolean eleccion) {
       vistagraficaperro.getAceptar().setEnabled(eleccion);
       vistagraficaperro.getCancelar().setEnabled(eleccion);
       vistagraficaperro.getLimpiar().setEnabled(eleccion);
    }
 
//Devuelve la aplicación a su estado original.
    private void cancelarTodo(){
        activarDesactivarBotones(true);
        activarDesactivarAceptarCancelar(false);
        vistagraficaperro.getAceptar().setVisible(true);
//        vistagraficaperro.getComprobarId().setVisible(false);
        activarDesactivarCamposTexto(false);
        activarDesactivarLeer(false);

        vistagraficaperro.getTextFieldIdPerro().setText("ID");
        vistagraficaperro.getTextFieldNombre().setText("Nombre");
        vistagraficaperro.getTextFieldNChip().setText("NChip(15Digitos)");
        vistagraficaperro.getTextFieldRaza().setText("Raza");
    }
  
    private void settearCampos() {
        vistagraficaperro.getTextFieldIdPerro().setText("ID");
        vistagraficaperro.getTextFieldNombre().setText("Nombre");
        vistagraficaperro.getTextFieldNChip().setText("NChip(15Digitos)");
        vistagraficaperro.getTextFieldRaza().setText("Raza");
    
    }
    private void limpiar(){
        vistagraficaperro.getTextFieldNombre().setText("");
        vistagraficaperro.getTextFieldNChip().setText("");
        vistagraficaperro.getTextFieldRaza().setText("");
    }
    
//    private void comprobarId() {
//
//    ArrayList arrayListComprobarId = imodelo.readPerro();
//    Iterator perros = arrayListComprobarId.iterator(); 
//    
//    while (perros.hasNext()) { 
//      perro = (Perro) perros.next(); 
//      String id = vistagraficaperro.getTextFieldIdPerro().getText();
//      
//      if (id.equals(perro.getIdPerro())) {
//        JOptionPane.showMessageDialog(vistagraficaperro.getFrame(),"ID encontrada, escoja otra.");
//        comprobarId = false;
//        } 
//     }
//    if (comprobarId) {
//        JOptionPane.showMessageDialog(vistagraficaperro.getFrame(),"ID no encontrada");
//        vistagraficaperro.getComprobarId().setVisible(false);
//        vistagraficaperro.getTextFieldIdPerro().setEditable(false);
//        vistagraficaperro.getAceptar().setVisible(true);
//        }
//    comprobarId = true;
//       
//    }
    
    private void comprobacionCamposVacios() {
        String id = vistagraficaperro.getTextFieldIdPerro().getText();
        String nombre = vistagraficaperro.getTextFieldNombre().getText();
        String nchip = vistagraficaperro.getTextFieldNChip().getText();
        String raza = vistagraficaperro.getTextFieldRaza().getText();
        noCampoVacio = true;
        
        while (id.equals("") || nombre.equals("") || nchip.equals("") || raza.equals("")) {
            JOptionPane.showMessageDialog(vistagraficaperro.getFrame(),"Uno de los campos está"
                    + " vacío, no se puede continuar.");
            noCampoVacio = false;
            break;
        }
    }
    
     private void comprobacionMail() {
       String nChip = vistagraficaperro.getTextFieldNChip().getText();
        Pattern pattern = Pattern.compile("\\d{15}");
        Matcher matcher = pattern.matcher(nChip);
        campoMailCorrecto = true;
        if (!matcher.matches()) {
            JOptionPane.showMessageDialog(vistagraficaperro.getFrame(),"El número de chip no es correcto, "
                    + "ha de ser 15 dígitos cualesquiera");
            campoMailCorrecto = false;
        }
    
    }
     
      private void anterior()  {
        ArrayList arrayListMover = imodelo.readPerro();
        Iterator perros = arrayListMover.iterator(); 
    
        while (perros.hasNext()) { 
        perro = (Perro) perros.next(); 
        String id = vistagraficaperro.getTextFieldIdPerro().getText();
      
            if (id.equals(perro.getIdPerro())) {
                  posicion = arrayListMover.indexOf(perro);
              } 
         }
        if (posicion != 0) {
            posicion -= 1;
            perro_indice = ((Perro)arrayListMover.get(posicion));
            vistagraficaperro.getComboLeer().setSelectedIndex(posicion);
            vistagraficaperro.getTextFieldIdPerro().setText(perro_indice.getIdPerro());
            vistagraficaperro.getTextFieldNombre().setText(perro_indice.getNombre());
            vistagraficaperro.getTextFieldNChip().setText(perro_indice.getNChip());
            vistagraficaperro.getTextFieldRaza().setText(perro_indice.getRaza());
        } else {
            posicion = arrayListMover.size() - 1;
            perro_indice = ((Perro)arrayListMover.get(posicion));
            vistagraficaperro.getComboLeer().setSelectedIndex(posicion);
            vistagraficaperro.getTextFieldIdPerro().setText(perro_indice.getIdPerro());
            vistagraficaperro.getTextFieldNombre().setText(perro_indice.getNombre());
            vistagraficaperro.getTextFieldNChip().setText(perro_indice.getNChip());
            vistagraficaperro.getTextFieldRaza().setText(perro_indice.getRaza());
        }
    }
      
     private void siguiente()  {
                ArrayList arrayListMover = imodelo.readPerro();
        Iterator perros = arrayListMover.iterator(); 
    
        while (perros.hasNext()) { 
        perro = (Perro) perros.next(); 
        String id = vistagraficaperro.getTextFieldIdPerro().getText();
      
            if (id.equals(perro.getIdPerro())) {
                  posicion = arrayListMover.indexOf(perro);
              } 
         }
        if (posicion != arrayListMover.size() - 1) {
            posicion += 1;
            perro_indice = ((Perro)arrayListMover.get(posicion));
            vistagraficaperro.getComboLeer().setSelectedIndex(posicion);
            vistagraficaperro.getTextFieldIdPerro().setText(perro_indice.getIdPerro());
            vistagraficaperro.getTextFieldNombre().setText(perro_indice.getNombre());
            vistagraficaperro.getTextFieldNChip().setText(perro_indice.getNChip());
            vistagraficaperro.getTextFieldRaza().setText(perro_indice.getRaza());
        } else {
            posicion = 0;
            perro_indice = ((Perro)arrayListMover.get(posicion));
            vistagraficaperro.getComboLeer().setSelectedIndex(posicion);
            vistagraficaperro.getTextFieldIdPerro().setText(perro_indice.getIdPerro());
            vistagraficaperro.getTextFieldNombre().setText(perro_indice.getNombre());
            vistagraficaperro.getTextFieldNChip().setText(perro_indice.getNChip());
            vistagraficaperro.getTextFieldRaza().setText(perro_indice.getRaza());
        }
    }
     
     
     private void primer()  {
        ArrayList arrayListMover = imodelo.readPerro();
        
        if (arrayListMover != null) {
            posicion = 0;
            perro_indice = ((Perro)arrayListMover.get(posicion));
            vistagraficaperro.getComboLeer().setSelectedIndex(posicion);
            vistagraficaperro.getTextFieldIdPerro().setText(perro_indice.getIdPerro());
            vistagraficaperro.getTextFieldNombre().setText(perro_indice.getNombre());
            vistagraficaperro.getTextFieldNChip().setText(perro_indice.getNChip());
            vistagraficaperro.getTextFieldRaza().setText(perro_indice.getRaza());
        } else {
            perro_indice = null;
            posicion = -1;
           
        }
    }
     
     private void ulti()  {
            ArrayList arrayListMover = imodelo.readPerro(); 
            posicion = (arrayListMover.size()-1);
            perro_indice = ((Perro)arrayListMover.get(posicion));
            vistagraficaperro.getComboLeer().setSelectedIndex(posicion);
            vistagraficaperro.getTextFieldIdPerro().setText(perro_indice.getIdPerro());
            vistagraficaperro.getTextFieldNombre().setText(perro_indice.getNombre());
            vistagraficaperro.getTextFieldNChip().setText(perro_indice.getNChip());
            vistagraficaperro.getTextFieldRaza().setText(perro_indice.getRaza());
    }
     
   @Override
   public void actionPerformed (ActionEvent event) { // Metodos implementados por ActionListener que vigila los botones
      if (vistagraficaperro.getCreate() == event.getSource()) {
        
        activarDesactivarAceptarCancelar(true);
        activarDesactivarBotones(false);
        activarDesactivarCamposTexto(true);
        activarDesactivarLeer(false);
 
//        vistagraficaperro.getComprobarId().setVisible(true);
        vistagraficaperro.getAceptar().setVisible(true);
        opcionEscogida = "crear";
        
      }
      else if (vistagraficaperro.getRead() == event.getSource()) {
         no_actualizar = false;
          activarDesactivarLeer(true);
//        activarDesactivarBotones(false);
        vistagraficaperro.getCancelar().setEnabled(true);
        vistagraficaperro.getComboLeer().removeAllItems();
        read();
        llenadoTextBoxConComboBoxPerro();
        opcionEscogida = "leer";
       no_actualizar = true;
      }
      else if (vistagraficaperro.getUpdate() == event.getSource()) {
        activarDesactivarAceptarCancelar(true);
        activarUpdateDelete(true);
        activarDesactivarBotones(false);
        opcionEscogida = "actualizar";
        
      }
      else if (vistagraficaperro.getDelete() == event.getSource()) {
        activarDesactivarAceptarCancelar(true);
        activarUpdateDelete(true);
        activarDesactivarBotones(false);
        opcionEscogida = "borrar";
      }
//      else if (vistagraficaperro.getComprobarId() == event.getSource()) {
//        
//      }
      else if (vistagraficaperro.getExit() == event.getSource()) {
         vistagraficaperro.dispose();
      }
       else if (vistagraficaperro.getAceptar() == event.getSource()) {
         switch (opcionEscogida) {
             case "crear":
                 no_actualizar = false;
                 comprobacionCamposVacios();
                    if (!noCampoVacio) {
                    return;}
                 comprobacionMail();
                     if (!campoMailCorrecto) {
                        return;}
                 create();
                 vaciarTodosCampos();
                 no_actualizar = true;
                 break;
             case "actualizar":
                 update();
                 break;
             case "borrar":
                 delete();
                 break;    
            }
         }
      else if (vistagraficaperro.getLimpiar() == event.getSource()) {
        limpiar();
      }
      else if (vistagraficaperro.getCancelar() == event.getSource()) {
        cancelarTodo();
      }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (vistagraficaperro.getBotonIzq() == e.getSource()) {
        anterior();          
      }
      
      else if (vistagraficaperro.getBotonDer() == e.getSource()) {
        siguiente();
      }
      else if (vistagraficaperro.getBotonPri() == e.getSource()) {
        primer();
      }
      else if (vistagraficaperro.getBotonUlt() == e.getSource()) {
        ulti();
      }
    }

    @Override
    public void mousePressed(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (vistagraficaperro.getComboLeer() == e.getSource()) {
            if (no_actualizar) {
        llenadoTextBoxConComboBoxPerro();
            }
        }
      }
    }
